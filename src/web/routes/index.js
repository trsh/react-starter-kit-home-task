import React from 'react';
import { Switch, Route } from 'react-router-dom';
import PropTypes from 'prop-types';

// Templates
import TemplateNothing from '../components/Templates/Nothing';
import TemplateSidebar from '../components/Templates/Sidebar';

// Routes
import Home from '../components/Home';

import RecipesContainer from '../../containers/Recipes';
import RecipeContainer from '../../containers/Recipe';
import RecipeListingComponent from '../components/Recipe/Listing';
import RecipeSingleComponent from '../components/Recipe/Single';
import RecipeEditComponent from '../components/Recipe/Edit';

import SignUpContainer from '../../containers/SignUp';
import SignUpComponent from '../components/User/SignUp';

import LoginContainer from '../../containers/Login';
import LoginComponent from '../components/User/Login';

import ForgotPasswordContainer from '../../containers/ForgotPassword';
import ForgotPasswordComponent from '../components/User/ForgotPassword';

import UpdateProfileContainer from '../../containers/UpdateProfile';
import UpdateProfileComponent from '../components/User/UpdateProfile';

import Error from '../components/UI/Error';

const Index = ({store}) => 
{

  // Tempral & cheap solution for task :)
  const member = store.getState().member;

  return (
    <Switch>
      <Route
        exact
        path="/"
        render={props => (
          <TemplateSidebar>
            <Home {...props} />
          </TemplateSidebar>
        )}
      />
      <Route
        path="/sign-up"
        render={props => (
          <TemplateNothing pageTitle="Sign Up">
            <SignUpContainer {...props} Layout={SignUpComponent} />
          </TemplateNothing>
        )}
      />
      <Route
        path="/login"
        render={props => (
          <TemplateNothing pageTitle="Login">
            <LoginContainer {...props} Layout={LoginComponent} />
          </TemplateNothing>
        )}
      />
      <Route
        path="/forgot-password"
        render={props => (
          <TemplateNothing pageTitle="Forgot Password">
            <ForgotPasswordContainer {...props} Layout={ForgotPasswordComponent} />
          </TemplateNothing>
        )}
      />
      <Route
        path="/update-profile"
        render={props => (
          <TemplateSidebar pageTitle="Update Profile">
            <UpdateProfileContainer {...props} Layout={UpdateProfileComponent} />
          </TemplateSidebar>
        )}
      />
      <Route
        path="/recipes"
        render={props => (
          <TemplateSidebar pageTitle="Recipes">
            <RecipesContainer {...props} Layout={RecipeListingComponent} />
          </TemplateSidebar>
        )}
      />
      <Route
        path="/recipe/:id"
        render={props => (
          <TemplateSidebar pageTitle="Recipe View">
            <RecipesContainer {...props} Layout={RecipeSingleComponent} />
          </TemplateSidebar>
        )}
      />
      <Route
        path="/recipe-edit/:id"
        render={props => (member && member.uid) ? (
          <TemplateSidebar pageTitle="Edit Recipe">
            <RecipeContainer {...props} Layout={RecipeEditComponent} />
          </TemplateSidebar>
        ) : <Error {...props} title="404" content="Sorry, the route you requested does not exist" />}
      />
      <Route
        path="/recipe-new"
        render={props => (member && member.uid) ? (
          <TemplateSidebar pageTitle="Create Recipe">
            <RecipeContainer {...props} Layout={RecipeEditComponent} />
          </TemplateSidebar>
        ) : <Error {...props} title="404" content="Sorry, the route you requested does not exist" />}
      />
      <Route
        render={props => (
          <TemplateSidebar pageTitle="404 - Page not found">
            <Error {...props} title="404" content="Sorry, the route you requested does not exist" />
          </TemplateSidebar>
        )}
      />
    </Switch>
)};

Index.propTypes = {
  store: PropTypes.shape({}).isRequired
};

export default Index;

