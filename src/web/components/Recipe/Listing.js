import React from 'react';
import PropTypes from 'prop-types';
import {
  Row,
  Col,
  Card,
  CardImg,
  CardText,
  CardBody,
  CardTitle,
} from 'reactstrap';
import { Link } from 'react-router-dom';
import Error from '../UI/Error';

const RecipeListing = ({ error, loading, recipes, member }) => {
  // Error
  if (error) return <Error content={error} />;

  const loggedIn = !!(member && member.uid);

  // Build Cards for Listing
  const cards = recipes.map(item => (
    <Card key={`${item.id}`}>
      <Link to={`/recipe/${item.id}`}>
        <CardImg top src={item.image} alt={item.title} />
      </Link>
      <CardBody>
        <CardTitle>{item.title}</CardTitle>
        <CardText>{item.body}</CardText>
        <Link className="btn btn-primary" to={`/recipe/${item.id}`}>
          View Recipe
          {' '}
          <i className="icon-arrow-right" />
        </Link> 
        <br></br>
        {loggedIn && (
        <Link className="btn btn-warning mt-2" to={`/recipe-edit/${item.id}`}>
          Edit Recipe
          {' '}
          <i className="icon-arrow-right" />
        </Link>)}
      </CardBody>
    </Card>
  ));

  // Show Listing
  return (
    <div>
      <Row className="pt-4 pt-sm-0">
        <Col sm="10">
          <h1>Recipes</h1>
          <p>The following data is read directly from Firebase.</p>
        </Col>
        <Col sm="2">
          {loggedIn && (
          <Link className="btn btn-warning" to={`/recipe-new`}>
            New Recipe
          </Link>)}
        </Col>
      </Row>
      <Row className={loading ? 'content-loading' : ''}>
        <Col sm="12" className="card-columns">{cards}</Col>
      </Row>
    </div>
  );
};

RecipeListing.propTypes = {
  error: PropTypes.string,
  loading: PropTypes.bool.isRequired,
  recipes: PropTypes.arrayOf(PropTypes.shape()).isRequired,
  member: PropTypes.shape({}).isRequired
};

RecipeListing.defaultProps = {
  error: null,
};

export default RecipeListing;
