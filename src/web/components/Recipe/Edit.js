import React from 'react';
import PropTypes from 'prop-types';
import {
  Row,
  Col,
  Card,
  CardText,
  CardBody,
  CardHeader,
  ListGroup,
  ListGroupItem,
  FormGroup,
  Label,
  Input,
  Form,
  Button,
  InputGroup,
  Alert,
} from 'reactstrap';
import { Helmet } from 'react-helmet';
import { Link } from 'react-router-dom';
import { errorMessages } from '../../../constants/messages';
import Loading from '../UI/Loading';
import Error from '../UI/Error';

class RecipeEdit extends React.Component {
  static propTypes = {
    error: PropTypes.string,
    success: PropTypes.string,
    loading: PropTypes.bool.isRequired,
    recipe: PropTypes.shape({}).isRequired,
    onFormSubmit: PropTypes.func.isRequired,
    meals: PropTypes.arrayOf(PropTypes.shape()).isRequired,
  }

  static defaultProps = {
    error: null,
    success: null,
  }

  static defaultState = {
    id: null,
    title: '',
    author: '',
    body: '',
    ingredients: [],
    method: [],
    category: null,
    image: '',
  }

  constructor(props) {
    super(props);
    this.state = RecipeEdit.defaultState;
  }

  // Lint error
  /* componentDidUpdate() {
    const { recipe, loading } = this.props;
    const { id } = this.state;

    if (!loading && !id && (recipe && recipe.id)) {
      const newState = {};
      Object.keys(RecipeEdit.defaultState).forEach((key) => {
        newState[key] = recipe[key];
      });

      this.setState(newState);
    }
  } */

  static getDerivedStateFromProps = (props, state) => {
    const { recipe, loading, meals } = props;
    const { id, category } = state;

    if (!loading && !id && (recipe && recipe.id)) {
      const newState = {};
      Object.keys(RecipeEdit.defaultState).forEach((key) => {
        newState[key] = recipe[key];
      });

      return newState;
    }
    if (!category && (meals && meals.length)) {
      return { category: meals[0].id };
    }

    return null;
  }

  handleSubmit = (e) => {
    e.preventDefault();
    const { onFormSubmit } = this.props;
    onFormSubmit(this.state).catch(() => {});
  }

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  }

  handleAddIngredient = () => {
    const { ingredients } = this.state;
    this.setState({
      ingredients: (ingredients || []).concat(['']),
    });
  };

  handleAddMethod = () => {
    const { method } = this.state;
    this.setState({
      method: (method || []).concat(['']),
    });
  };

  handleRemoveIngredient = idx => () => {
    const { ingredients } = this.state;
    this.setState({
      ingredients: ingredients.filter((s, sidx) => idx !== sidx),
    });
  };

  handleRemoveMethod = idx => () => {
    const { method } = this.state;
    this.setState({
      method: method.filter((s, sidx) => idx !== sidx),
    });
  };

  handleIngredientListChange = (index, event) => {
    const { ingredients } = this.state;
    ingredients[index] = event.target.value;
    this.setState({ ingredients });
  }

  handleMethodListChange = (index, event) => {
    const { method } = this.state;
    method[index] = event.target.value;
    this.setState({ method });
  }

  render() {
    const {
      loading, error, success, meals,
    } = this.props;

    if (loading) return <Loading />;

    // Recipe not found (special case - redirect)
    if (error === errorMessages.recipe404) return <Error content={errorMessages.recipe404} />;

    const {
      title, author, body, ingredients, method, image, category,
    } = this.state;

    return (
      <div>
        <Helmet>
          <title>
Edit:
            {title}
          </title>
        </Helmet>

        {!!error && <Alert color="danger">{error}</Alert>}
        {!!success && <Alert color="success">{success}</Alert>}

        <Form onSubmit={this.handleSubmit}>
          <Row className="pt-4 pt-sm-0">
            <Col sm="12">
              <FormGroup>
                <Label for="title">Category</Label>

                <Input type="select" name="category" id="category" disabled={loading} value={category || ''} onChange={this.handleChange}>
                  {meals.map(item => (
                    <option value={item.id} key={item.id}>{item.title}</option>
                  ))}
                </Input>

              </FormGroup>

              <FormGroup>
                <Label for="image">Image url</Label>
                <Input
                  type="text"
                  name="image"
                  id="image"
                  placeholder="www something"
                  disabled={loading}
                  value={image}
                  onChange={this.handleChange}
                />
              </FormGroup>

              {/* TOOD: slug field */}

              <FormGroup>
                <Label for="title">Title</Label>
                <Input
                  type="text"
                  name="title"
                  id="title"
                  placeholder="Pudding"
                  disabled={loading}
                  value={title}
                  onChange={this.handleChange}
                />
              </FormGroup>

              <FormGroup>
                <Label for="author">Author</Label>
                <Input
                  type="text"
                  name="author"
                  id="author"
                  placeholder="John"
                  disabled={loading}
                  value={author}
                  onChange={this.handleChange}
                />
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Col lg="4" className="recipe-view-card">
              <Card>
                <CardHeader>About this recipe</CardHeader>
                <CardBody>
                  <CardText>
                    <Input
                      type="textarea"
                      rows="10"
                      name="body"
                      id="body"
                      placeholder="Bla bla"
                      disabled={loading}
                      value={body}
                      onChange={this.handleChange}
                    />
                  </CardText>
                </CardBody>
              </Card>
            </Col>
            <Col lg="4" className="recipe-view-card">
              <Card>
                <CardHeader>Ingredients</CardHeader>
                <ListGroup className="list-group-flush">

                  {(ingredients || []).map((item, index) => (
                    <ListGroupItem key={index}>
                      <InputGroup>
                        <Input
                          type="text"
                          placeholder=""
                          disabled={loading}
                          value={item}
                          onChange={e => this.handleIngredientListChange(index, e)}
                        />
                        <span className="input-group-btn">
                          <button
                            type="button"
                            onClick={this.handleRemoveIngredient(index)}
                            className="small"
                          >
            -
                          </button>
                        </span>
                      </InputGroup>
                    </ListGroupItem>
                  ))}

                </ListGroup>
                <Button color="secondary" disabled={loading} onClick={this.handleAddIngredient}>
                  {loading ? 'Loading' : 'Add'}
                </Button>
              </Card>
            </Col>
            <Col lg="4" className="recipe-view-card">
              <Card>
                <CardHeader>Method</CardHeader>
                <ListGroup className="list-group-flush">

                  {(method || []).map((item, index) => (
                    <ListGroupItem key={index}>
                      <InputGroup>
                        <Input
                          rows="3"
                          type="textarea"
                          placeholder=""
                          disabled={loading}
                          value={item}
                          onChange={e => this.handleMethodListChange(index, e)}
                        />
                        <span className="input-group-btn">
                          <button
                            type="button"
                            onClick={this.handleRemoveMethod(index)}
                            className="small"
                          >
              -
                          </button>
                        </span>
                      </InputGroup>
                    </ListGroupItem>
                  ))}

                </ListGroup>
                <Button color="secondary" disabled={loading} onClick={this.handleAddMethod}>
                  {loading ? 'Loading' : 'Add'}
                </Button>
              </Card>
            </Col>
          </Row>
          <Row className="pt-5 pb-3">
            <Col sm="12">
              <Link className="btn btn-secondary" to="/recipes">
                <i className="icon-arrow-left" />
                {' '}
                Back
              </Link>

              <Button color="primary" className="float-right" disabled={loading}>
                {loading ? 'Loading' : 'Save'}
              </Button>

            </Col>
          </Row>
        </Form>

      </div>
    );
  }
}

export default RecipeEdit;
