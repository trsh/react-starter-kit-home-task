import { Firebase, FirebaseRef } from '../lib/firebase';
import { errorMessages } from '../constants/messages';
// import initState from '../store/recipes';

export default {
  /**
   *  Initial state
   */
  state: {}, // initial state

  /**
   * Reducers
   */
  reducers: {
    setRecipeDetails(_state, payload) {
      const {
        id, title, body, category, image, author, ingredients, method,
      } = payload;

      return {
        id, title, body, category, image, author, ingredients, method,
      };
    },

    resetRecipe() {
      return {};
    },
  },

  /**
   * Effects/Actions
   */
  effects: () => ({
    /**
     * Store recipe
     *
     * @param {int} rid
     * @param {obj} state
     * @param {obj} formData
     * @return {Promise}
     */
    storeRecipe(rid, _state, formData) {
      const {
        title, body, author, category, image,
      } = formData;

      let {
        ingredients, method,
      } = formData;

      return new Promise(async (resolve, reject) => {
        // Validation rules
        if (!category) return reject({ message: errorMessages.missingRecipeCategory });
        if (!image) return reject({ message: errorMessages.missingRecipeImage });
        if (!title) return reject({ message: errorMessages.missingRecipeTitle });
        if (!author) return reject({ message: errorMessages.missingRecipeAuthor });   
        if (!body) return reject({ message: errorMessages.missingRecipeBody });                     

        ingredients = ingredients || [];
        method = method || [];

        if (rid) {
          return FirebaseRef.child('recipes').orderByChild('id').equalTo(rid)
            .once('value', (snapshot) => {
              let found = null;

              snapshot.forEach((item) => {
                const val = item.val();
                if (val.id === rid) {
                  found = item;
                }
              });

              if (found === null) {
                reject({ message: errorMessages.recipe404 });
              }

              FirebaseRef.child(`recipes/${found.key}`).update({
                title, body, author, ingredients, method, category, image,
              }).then(async () => {
                resolve();
              }).catch(reject);
            })
            .catch(reject);
        }
        return FirebaseRef.child('recipes').orderByChild('id').limitToLast(1)
          .once('value', (snapshot) => {
            let lastId = null;

            snapshot.forEach((item) => {
              const val = item.val();
              lastId = val.id;
            });

            if (lastId === null) {
              reject({ message: errorMessages.recipe404 });
            }

            FirebaseRef.child('recipes').push({
              title, body, author, ingredients, method, id: lastId + 1, category, image,
            }).then(async () => {
              resolve();
            }).catch(reject);
          })
          .catch(reject);

        /* FirebaseRef.child(`counter`).transaction(function(currentData){
              console.log(currentData);
              return;
            }); TODO:  */
      }).catch((err) => { throw err.message; });
    },

    /**
      * Flush Recipe
      *
      */
    flushRecipe() {
      this.resetRecipe();
    },

    /**
      * Get Recipe
      *
      * @param {int} rid
      * @return {Promise}
      */
    getRecipe(rid) {
      if (Firebase === null) return () => new Promise(resolve => resolve());

      return new Promise((resolve, reject) => FirebaseRef.child('recipes').orderByChild('id').equalTo(rid)
        .once('value', (snapshot) => {
          let found = {};

          snapshot.forEach((item) => {
            const val = item.val();
            if (val.id === rid) {
              found = val;
            }
          });

          if (!found.id) {
            return reject({ message: errorMessages.recipe404 });
          }

          return resolve(found);
        })).catch((err) => { throw err.message; });
    },
  }),
};
