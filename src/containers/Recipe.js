import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

class Recipe extends Component {
  static propTypes = {
    Layout: PropTypes.func.isRequired,
    match: PropTypes.shape({ params: PropTypes.shape({}) }),
    fetchRecipe: PropTypes.func.isRequired,
    onFormSubmit: PropTypes.func.isRequired,
    fetchMeals: PropTypes.func.isRequired,
    meals: PropTypes.arrayOf(PropTypes.shape()).isRequired,
  }

  static defaultProps = {
    match: null,
  }

  state = {
    error: null,
    loading: true,
    success: null,
    recipe: {},
  }

  componentDidMount = () => this.fetchData();

  componentDidUpdate = (prevProps) => {
    const currId = this.idFromProps(this.props);
    const prevId = this.idFromProps(prevProps);

    if (currId !== prevId) {
      this.fetchData();
    }
  }

  idFromProps = (props) => {
    const { match } = props;
    return (match && match.params && match.params.id) ? match.params.id : null;
  }

  fetchData = () => {
    const { fetchRecipe, fetchMeals } = this.props;
    const rid = this.idFromProps(this.props);

    if (rid) {
      this.setState({ loading: true });

      return fetchMeals()
        .then(() => fetchRecipe(parseInt(rid, 10)))
        .then((recipe) => {
          this.setState({
            loading: false,
            error: null,
            recipe,
          });
        }).catch(err => this.setState({
          loading: false,
          error: err,
          recipe: {},
        }));
    }

    this.setState({ loading: false, recipe: {} });
    return new Promise(resolve => resolve);
  }

  onFormSubmit = (data) => {
    const { onFormSubmit } = this.props;
    const { recipe } = this.state;

    this.setState({ loading: true });

    const rid = (recipe && recipe.id) ? parseInt(recipe.id, 10) : null;

    return onFormSubmit(rid, data)
      .then(() => this.setState({
        loading: false,
        success: rid ? 'Success - Updated' : 'Success - Created', // TODO: from messages
        error: null,
      })).catch(err => this.setState({
        loading: false,
        success: null,
        error: err,
      }));
  }

  render = () => {
    const { Layout, meals } = this.props;
    const {
      loading, error, success, recipe,
    } = this.state;

    return (
      <Layout
        error={error}
        success={success}
        loading={loading}
        recipe={recipe}
        meals={meals}
        onFormSubmit={this.onFormSubmit}
        reFetch={() => this.fetchData()}
      />
    );
  }
}

const mapStateToProps = state => ({
  meals: state.recipes.meals || {},
});

const mapDispatchToProps = dispatch => ({
  fetchRecipe: dispatch.recipe.getRecipe,
  onFormSubmit: dispatch.recipe.storeRecipe,
  fetchMeals: dispatch.recipes.getMeals,
});

export default connect(mapStateToProps, mapDispatchToProps)(Recipe);
