Galvenie faili kur skatīties:

src\web\components\Recipe\Edit.js
src\models\recipe.js
src\containers\Recipe.js

Kam nepietika laika:

Pa lielam viss ap src\models\recipe.js ir druksu Lame. Tur vahadzēja nevis ar id's, bet ar key's taisīt selektēšanu/updeitošanu uz DB. Šobrīd
sanāk kompilcēti un neefektīvi. Nākamo id jaunam ierakstam vajadzēt gūt caur transakciju, savādak var sakrist dubultie. Kā ārī varbūt DB pusē
kādus Rules, kas nepieļauj dublikātus.

src\web\components\Recipe\Edit.js pietrūkst slug lauks, kas ir oriģinālajā struktūrā. 

src\containers\Recipe.js nezinu vai šis ar līdz galam ir gudri. Pietrūkst mazliet zināšanas par Redux, jo iepriekšējos projekots, kur izmantoju
React lietoju MobX. Varbūt vispār nevajadzēja šo skriptu, un ņemt recepies no src\containers\Recipes.js. Nezinu, vispār jau kā tāds muļķīgs tas 
recepšu saraksts, jo parasti tādas lietas neupdeito LIVE par katru izmainītu burtu (tas drīzak kādiem čatiem, utt). So pieturoties pie savas loģikas un esošā koda sanāk tāds kā dīvains MIX :)

src\web\routes\index.js var redzēt primitīvu aizsardzību uz URL autorizēšanu. Lielākam projektam gan jau vajag kaut ko nopietnāku.

Kopumā izskatās ka padziļināti jāpapēta Firbase lietas un Redux, lai būtu uz 'Tu' ar šim tehnoloģijām. Droši vien ka palīdzētu kāds esoš (ejošs)projekts, kur var pasmelties idejas un 'common practices'. Bet nu paralēli es jau lasu šobrīd un apgūstu. Varbūt iesākumā arī man vajadzētu strādat pie esošiem projektiem un veikt izmaiņas, un tad uzticēt tādus no zerro. Vieglāk arī strādāt, ka ir konkrētas biznesa prasības.

Natvive daļu es neaiztiku pagaidām. Laikam jau priekš vieglā mājas darba pietiks ar šo...



